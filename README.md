First:
In order to build this file
-     Open Visual Studio.
-     Click on "File" -> "Open" -> "Project/Solution".
-     Navigate to the location where the solution file is stored and select it.
-     The solution should now be loaded in Visual Studio.
-     To build the solution, click on "Build" -> "Build Solution" in the top menu **or** use the shortcut key "Ctrl + Shift + B".
-     The build output will be displayed in the "Output" window at the bottom of the screen.
-     If there are any errors, they will be displayed in the "Error List" window.


Then:
Go to terminal and run the following command in order to test the product
    **./run_test.cmd**
